#! /bin/bash

set -e

if [[ -z "${CONTAINER_TEST_IMAGE}" ]]; then
    CONTAINER_TEST_IMAGE=test-image
fi

if [[ -z "${CONTAINER_NAME}" ]]; then
    CONTAINER_NAME=test-container
fi

sh ./run-tests.sh "${CONTAINER_NAME}" "${CONTAINER_TEST_IMAGE}"
