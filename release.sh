#! /usr/bin/env

set -e

if [ -z "${CI_COMMIT_TAG}" ]; then
  RELEASE_TAG=${CI_COMMIT_REF_SLUG}
else
  RELEASE_TAG=${CI_COMMIT_TAG}
fi

docker pull ${CONTAINER_TEST_IMAGE}
docker tag ${CONTAINER_TEST_IMAGE} ${CONTAINER_RELEASE_IMAGE_NAME}:${RELEASE_TAG}
docker push ${CONTAINER_RELEASE_IMAGE_NAME}:${RELEASE_TAG}

docker tag ${CONTAINER_RELEASE_IMAGE_NAME}:${RELEASE_TAG} ${CONTAINER_RELEASE_IMAGE_NAME}:latest
docker push ${CONTAINER_RELEASE_IMAGE_NAME}:latest
