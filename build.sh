#! /usr/bin/env

set -e

if [[ -z "${CONTAINER_TEST_IMAGE}" ]]; then
    CONTAINER_TEST_IMAGE=test-image
fi

if [[ -z "${CI_COMMIT_TAG}" ]]; then
  FROM_TAG="alpine"
else
  FROM_TAG=${CI_COMMIT_TAG}
fi

str="
  s!%%FROM_TAG%%!${FROM_TAG}!g;
"

sed "$str" container/Dockerfile.template > container/Dockerfile

docker build -t ${CONTAINER_TEST_IMAGE} container
docker push ${CONTAINER_TEST_IMAGE}
