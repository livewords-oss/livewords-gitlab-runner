#!/bin/bash

pid=0

# SIGTERM-handler
term_handler() {
  echo "Killing Runner with PID $pid"
  if [[ $pid -ne 0 ]]; then
    kill -SIGTERM "$pid"
    wait "$pid"
  fi

  runner_token=$(grep "token = " /etc/gitlab-runner/config.toml | sed -e "s/.*token = //" -e 's/"//g')
  echo "Unregistering Runner with token: ${runner_token}"
  gitlab-runner unregister \
    --url https://gitlab.com/ci \
    --token "${runner_token}"

  echo "Shutdown complete"
  exit 143; # 128 + 15 -- SIGTERM
}

# gitlab-ci-multi-runner data directory
DATA_DIR="/etc/gitlab-runner"
CONFIG_FILE=${CONFIG_FILE:-$DATA_DIR/config.toml}
# custom certificate authority path
CA_CERTIFICATES_PATH=${CA_CERTIFICATES_PATH:-$DATA_DIR/certs/ca.crt}
LOCAL_CA_PATH="/usr/local/share/ca-certificates/ca.crt"

update_ca() {
  echo "Updating CA certificates..."
  cp "${CA_CERTIFICATES_PATH}" "${LOCAL_CA_PATH}"
  update-ca-certificates --fresh >/dev/null
}

if [[ -f "${CA_CERTIFICATES_PATH}" ]]; then
  # update the ca if the custom ca is different than the current
  cmp --silent "${CA_CERTIFICATES_PATH}" "${LOCAL_CA_PATH}" || update_ca
fi

if [[ -z "${RUNNER_NAME}" ]]; then
  RUNNER_NAME="gitlab-runner-$(date +%Y%m%d-%H%M%S)"
fi

# setup handlers:
# on callback, kill the last background process, which is `tail -f /dev/null` and execute the specified handler
trap 'kill ${!}; term_handler' SIGQUIT

set -f                      # avoid globbing (expansion of *).
array=(${GITLAB_CI_TOKEN})
for i in "${!array[@]}"; do
    echo "Registering with repo at position $i"
    gitlab-runner register \
      --non-interactive \
      --registration-token "${array[i]}" \
      --locked \
      --executor "docker" \
      --limit "2" \
      --output-limit "8192" \
      --docker-privileged \
      --docker-volumes "/var/run/docker.sock:/var/run/docker.sock"
done

# launch gitlab-ci-multi-runner passing all arguments
exec gitlab-runner "$@" &
pid="$!"

# wait forever
while true
do
  tail -f /dev/null & wait ${!}
done
