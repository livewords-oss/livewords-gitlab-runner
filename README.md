# livewords-gitlab-runner [![build status](https://gitlab.com/livewords-oss/livewords-gitlab-runner/badges/master/build.svg)](https://gitlab.com/livewords-oss/livewords-gitlab-runner/commits/master)

This repository contains the code to build a docker image to be used a GitLab Runner that automatically registers itself on start, and unregisters on stop.
We are running this project in Amazon Container Service and always get a healthy list of runners in our projects on gitlab.com.

The container is build on top of the [gitlab/gitlab-runner](https://hub.docker.com/r/gitlab/gitlab-runner/) version alpine.

The container is able to automatically unregister from GitLab because it traps the `SIGTERM` signal that docker sends it when `docker stop` is called.
This logic is placed in the docker entrypoint script that we adapted from from the [gitlab/gitlab-runner](https://hub.docker.com/r/gitlab/gitlab-runner/) project.

## Build the container

Container is automatically built on `master` using the latest `alpine` tagged version. If you want to specify a custom tag for the build you can commit a specific tag on the repository, the CI will then try to pull [gitlab/gitlab-runner](https://hub.docker.com/r/gitlab/gitlab-runner/) using the tag committed.

Containers tagged with `latest` always refer to the `master` branch.

## Use the container

```bash
docker login registry.gitlab.com

docker pull registry.gitlab.com/livewords-oss/livewords-gitlab-runner:latest

docker run -d \
  --name=my-gitlab-runner \
  -e GITLAB_CI_TOKEN="xxxx" \
  -e CI_SERVER_URL="https://gitlab.com/" \
  -e RUNNER_NAME="My new runner" \
  -e DOCKER_IMAGE="alpine:latest" \
  -e DOCKER_CACHE_DIR="path/to/cache/dir"
  -v /var/run/docker.sock:/var/run/docker.sock \
  registry.gitlab.com/livewords-oss/livewords-gitlab-runner:latest
```

If variable `GITLAB_CI_TOKEN` contains multiple tokens separated by spaces (e.g. `GITLAB_CI_TOKEN="token1 token2 tokenN"`), then the runner will register in all the respective repositories.

## Changes to this container

This container is automatically tested at every change. The testing happens in the `run_tests.sh` script.
Any change to the container should be covered by either adapting the existing tests or adding new ones.

## License

Licensed under The MIT License (MIT).

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.

## Copyright

Copyright (c) 2017 LiveWords B.V.
